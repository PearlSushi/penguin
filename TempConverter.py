'''
Created on Jun 30, 2020

@author: ewake
'''
def ftc(ye):
    return (ye-32)*5/9
def ctf(ya):
    return (ya*9/5)+32

running=True

while running==True:
    s = int(input("Are you converting F to C (1) or C to F (2)? "))
    if s==1:
        ye = int(input("What is the temperature in Fahrenheit?  "))
        print("Here is the temperature in Celcius: ")
        print(ftc(ye))
    if s==2:
        ya = int(input("What's the temperature in Celcius?  "))
        print("Here is your temperature in Fahrenheit:  ")
        print(ctf(ya))
    cont = input("Would you like to do more? Yes or no? ")
    if cont=="yes":
        print("Okay what's next?")
    if cont=="no":
        print("Okay, nice working with you!")
        break