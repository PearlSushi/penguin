'''
Created on Jul 2, 2020

@author: ewake
'''
class Card:
    def __init__(self,number,suit):
        self.suit=suit
        self.number=number
    def __str__(self):
        if self.number==11:
            return str("Jack")+" of "+self.suit
        if self.number==12:
            return str("Queen")+" of "+self.suit
        if self.number==13:
            return str("King")+" of "+self.suit
        if self.number==14:
            return str("Ace")+" of "+self.suit
        return str(self.number)+" of "+self.suit   
    def getnum(self):
        return self.number
    def value(self):
        if self.number>=11 and self.number<=13:
            return 10
        elif self.number==14:
            return 11
        else:
            return self.number











        