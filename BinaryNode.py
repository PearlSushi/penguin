'''
Created on Jul 6, 2021

@author: User
'''

class binarynode():
    def __init__(self,value):
        self.value = value
        self.lchild = None
        self.rchild = None
    def setv(self,value):
        self.value = value
    def getv(self):
        return self.value
    def addlc(self,value): 
        self.lchild = binarynode(value)
    def addrc(self,value):
        self.rchild = binarynode(value)
    def getlc(self):
        return self.lchild
    def getrc(self):
        return self.rchild
    def clearlc(self):
        self.lchild = None
    def clearrc(self):
        self.rchild = None

